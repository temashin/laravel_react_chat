<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Posts;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * add new post
     *
     * @param Illuminate\Http\Response
     * @return Illuminate\Http\Response
     */

    public function add( Request $request )
    {
        $Post = new Posts();
        $Post->title =$request->title;
        $Post->description = $request->description;
        $Post->author = $request->author;
        $Post->save();

        return $Post;

    }

    /**
     * update post
     *
     * @param Illuminate\Http\Response
     * @return Illuminate\Http\Response
     */
    public function update( Request $request )
    {
        $Post = Posts::find( $request->id );
        $Post->title =$request->title;
        $Post->description = $request->description;
        $Post->author = $request->author;
        $Post->save();
        return $Post;
    }

    /**
     *
     *
     * @param int $id
     *
     * @return Illuminate\Http\Response
     *
     *
     */
    public function delete ( $id )
    {
        $Post = Posts::find( $id );

        return ($Post->delete()) ? true : false;

    }


    /**
     *
     *
     * @param int $id
     *@return Illuminate\Http\Response
     */
    public function getPost( $id )
    {
        $Post = Posts::find( $id );

        return $Post;

    }



}
